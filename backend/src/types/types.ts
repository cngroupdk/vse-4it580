import { Connection } from 'mariadb';

export type CustomContext = {
  dbConnection: Connection;
  authUser: JWTPayload | null;
};

export type JWTPayload = {
  id: number;
  iat: number;
};
