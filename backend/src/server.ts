import { type Connection } from 'mariadb';
import { ApolloServer } from '@apollo/server';
import express from 'express';
import * as http from 'http';
import cors from 'cors';
import { ApolloServerPluginDrainHttpServer } from '@apollo/server/plugin/drainHttpServer';
import graphqlUploadExpress from 'graphql-upload/graphqlUploadExpress.js';
import {
  ExpressContextFunctionArgument,
  expressMiddleware,
} from '@apollo/server/express4';

import schemaDefinition from './graphql/rootTypeDefs';
import rootResolver from './graphql/rootResolver';
import { getConnection } from './libs/dbConnection';
import { CustomContext } from './types/types';
import { MOCKS, PORT } from './config';
import mockResolvers from '../mocks/mockResolvers';
import { parseAndVerifyJWT } from './libs/jwt';

const init = async () => {
  const app = express();

  const httpServer = http.createServer(app);

  const server = new ApolloServer({
    typeDefs: [schemaDefinition],
    resolvers: MOCKS ? mockResolvers : rootResolver,
    plugins: [ApolloServerPluginDrainHttpServer({ httpServer })],
  });

  await server.start();

  const customContext = async ({
    req,
  }: ExpressContextFunctionArgument): Promise<CustomContext> => {
    const authToken = req.headers.authorization || '';
    const authUser = parseAndVerifyJWT(authToken);

    return {
      dbConnection: MOCKS ? (null as any as Connection) : await getConnection(),
      authUser,
    };
  };

  app.use(
    '/graphql',
    cors<cors.CorsRequest>(), // accepts all origins ('*'), not support cookies
    express.json(),
    graphqlUploadExpress(),
    expressMiddleware(server, {
      context: customContext,
    }),
  );

  httpServer.listen({ port: PORT }, () => {
    console.log('Server listening on port: ' + PORT);
  });
};

init();
