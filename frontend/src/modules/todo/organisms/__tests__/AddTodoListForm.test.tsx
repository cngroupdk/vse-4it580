import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';

import { AddTodoListItemForm } from '../AddTodoListForm';

describe('AddTodoListItemForm', () => {
  it('calls `addItem` with the entered description', async () => {
    const user = userEvent.setup();
    const addItem = vi.fn();

    render(<AddTodoListItemForm addItem={addItem} />);

    expect(addItem).not.toBeCalled();

    const descriptionInput = screen.getByRole('textbox', {
      name: 'What needs to be done?',
    });
    const addButton = screen.getByRole('button', { name: 'Add' });

    await user.type(descriptionInput, 'Learn unit testing');
    await user.click(addButton);

    expect(addItem).toBeCalledWith({
      description: 'Learn unit testing',
      isCompleted: false,
    });
  });
});
