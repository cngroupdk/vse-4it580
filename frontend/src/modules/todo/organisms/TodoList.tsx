import { AnimatePresence, motion } from 'framer-motion';

import { Box, Stack } from 'src/shared/design-system';

import { TodoListItem, type TodoListItemProps } from '../molecules';
import { type TodoItem } from '../types';

export type TodoListProps = {
  items: Array<TodoItem>;
  onSetIsCompleted: TodoListItemProps['onSetIsCompleted'];
  onRemove: TodoListItemProps['onRemove'];
};

export function TodoList({ items, onSetIsCompleted, onRemove }: TodoListProps) {
  return (
    <Stack
      border="1px"
      borderColor="gray.300"
      borderRadius="md"
      overflow="hidden"
      spacing="0"
    >
      {items.length === 0 && (
        <Box p="4" textAlign="center" bg="gray.50">
          No items
        </Box>
      )}
      <AnimatePresence initial={false}>
        {items.map((item) => (
          <motion.div
            key={item.id}
            initial={{ scale: 0.8, opacity: 0, height: 0 }}
            animate={{ scale: 1, opacity: 1, height: 'auto' }}
            exit={{ scale: 0.5, opacity: 0, height: 0 }}
          >
            <TodoListItem
              id={item.id}
              isCompleted={item.isCompleted}
              onSetIsCompleted={onSetIsCompleted}
              onRemove={onRemove}
            >
              {item.description}
            </TodoListItem>
          </motion.div>
        ))}
      </AnimatePresence>
    </Stack>
  );
}
