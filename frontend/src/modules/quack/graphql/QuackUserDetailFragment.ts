import { type ResultOf } from '@graphql-typed-document-node/core';

import { type FragmentType, gql } from 'src/gql';

export const QuackUserDetailFragment = gql(/* GraphQL */ `
  fragment QuackUserDetail on User {
    id
    name
    userName
    profileImageUrl
    quacks {
      id
      ...BaseQuack
    }
  }
`);

export type QuackUserDetailFragmentType = FragmentType<
  typeof QuackUserDetailFragment
>;

export type QuackUserDetailResult = ResultOf<typeof QuackUserDetailFragment>;
