import {
  type ResultOf,
  type VariablesOf,
} from '@graphql-typed-document-node/core';

import { gql } from 'src/gql';

export const AddQuackMutation = gql(/* GraphQL */ `
  mutation AddQuack($userId: Int!, $text: String!) {
    addQuack(userId: $userId, text: $text) {
      id
    }
  }
`);

export type AddQuackMutationVariables = VariablesOf<typeof AddQuackMutation>;

export type AddQuackMutationResult = ResultOf<typeof AddQuackMutation>;
