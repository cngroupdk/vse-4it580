import { useFragment } from 'src/gql';
import { AuthUser } from 'src/modules/auth/auth-core';
import {
  AvatarPhoto,
  Button,
  ErrorBanner,
  Heading,
  Loading,
  ReloadButton,
} from 'src/shared/design-system';
import { MainSection, TopNavigation } from 'src/shared/navigation';

import {
  QuackUserDetailFragment,
  type QuackUserDetailFragmentType,
} from '../graphql/QuackUserDetailFragment';
import { QuackForm } from '../molecules';
import { QuackList } from '../organisms';

type Props = {
  userName?: string;
  userFragment: QuackUserDetailFragmentType | null;
  loading: boolean;
  error?: Error;
  onReload: () => void;
  quackFormState: {
    loading: boolean;
    error?: Error;
    text: string;
    setText: (text: string) => void;
    onSubmit: (data: { text: string }) => void;
  };
  currentUser: AuthUser | null;
};

export function UserDetailTemplate({
  userName,
  userFragment,
  loading,
  error,
  onReload,
  quackFormState,
  currentUser,
}: Props) {
  const showQuackForm =
    quackFormState && currentUser && currentUser.userName === userName;

  const user = useFragment(QuackUserDetailFragment, userFragment);

  return (
    <>
      <TopNavigation />
      <MainSection maxW="30rem">
        {loading && !user && <Loading />}

        {error && (
          <ErrorBanner title={error.message}>
            <Button colorScheme="red" onClick={onReload}>
              Reload
            </Button>
          </ErrorBanner>
        )}

        {user ? (
          <>
            <header>
              {user.profileImageUrl ? (
                <AvatarPhoto
                  src={user.profileImageUrl}
                  alt={user.name}
                  size="32"
                  mb="2"
                />
              ) : null}
              <Heading as="h2" m="0">
                {user.name}
              </Heading>
              <Heading as="h5" fontWeight="400" color="gray.500">
                @{user.userName}
              </Heading>
            </header>

            {showQuackForm && <QuackForm {...quackFormState} mt="2" />}

            <ReloadButton
              onClick={onReload}
              isLoading={loading}
              float="right"
            />

            <QuackList quacks={user.quacks} />
          </>
        ) : null}
      </MainSection>
    </>
  );
}
