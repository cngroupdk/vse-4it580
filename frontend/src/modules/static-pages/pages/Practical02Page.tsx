import { useSearchParams } from 'react-router-dom';

import { useTodoList } from 'src/modules/todo/hooks';
import { AddTodoListItemForm, TodoList } from 'src/modules/todo/organisms';
import { todoListStateParamName } from 'src/route';
import { Heading, Stack, Tab, TabList, Tabs } from 'src/shared/design-system';

const STATES = ['all', 'completed', 'not-completed'] as const;
const DEFAULT_STATE = STATES[0];

export type TodoListState = (typeof STATES)[number];

function getValidFilter(filterParam: string | null): TodoListState {
  return STATES.includes(filterParam as TodoListState)
    ? (filterParam as TodoListState)
    : DEFAULT_STATE;
}

export function Practical02Page() {
  const { items, addItem, setItemIsCompleted, removeItem } = useTodoList();
  const [searchParams, setSearchParams] = useSearchParams({
    [todoListStateParamName]: DEFAULT_STATE,
  });
  const filterParam = searchParams.get(todoListStateParamName);
  const filter = getValidFilter(filterParam);

  return (
    <Stack>
      <Heading>Practical 02</Heading>
      <AddTodoListItemForm addItem={addItem} />
      <Tabs
        index={STATES.indexOf(filter)}
        onChange={(index) => {
          setSearchParams((prevParams) => {
            prevParams.set(todoListStateParamName, STATES[index]);

            return prevParams;
          });
        }}
        variant="soft-rounded"
        colorScheme="blue"
        my="4"
      >
        <TabList>
          <Tab>All</Tab>
          <Tab>Completed</Tab>
          <Tab>Not completed</Tab>
        </TabList>
      </Tabs>
      <TodoList
        key={filter}
        items={items.filter((item) => {
          if (filter === 'completed') return item.isCompleted;
          if (filter === 'not-completed') return !item.isCompleted;
          return true;
        })}
        onSetIsCompleted={setItemIsCompleted}
        onRemove={removeItem}
      />
    </Stack>
  );
}
