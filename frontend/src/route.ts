import {
  Practical01Page,
  Practical02Page,
  Practical03Page,
  type TodoListState,
} from 'src/modules/static-pages';

export const route = {
  home: () => `/`,
  practical: (id: string) => `/practical/${id}`,
  todoList: (state?: TodoListState) => {
    const baseUrl = '/practical/02';

    if (!state) {
      return baseUrl;
    }

    const urlParams = new URLSearchParams({
      [todoListStateParamName]: state,
    });

    return `${baseUrl}?${urlParams}`;
  },
  about: () => `/about`,
  terms: () => `/terms`,
  signIn: () => `/auth/signin`,
  signUp: () => `/auth/signup`,
  userDetail: (userName: string) => `/${userName}`,
};

export const todoListStateParamName = 'filter';

export const PRACTICALS = [
  // Practical pages
  { id: '01', PageComponent: Practical01Page },
  { id: '02', PageComponent: Practical02Page },
  {
    id: '03',
    PageComponent: Practical03Page,
    wrapperProps: {
      maxW: '80rem',
      minW: 'none',
      w: '100%',
    },
  },
];
