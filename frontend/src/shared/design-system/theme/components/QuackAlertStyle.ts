import {
  createMultiStyleConfigHelpers,
  type StyleFunctionProps,
} from '@chakra-ui/react';

const { definePartsStyle, defineMultiStyleConfig } =
  createMultiStyleConfigHelpers(['container', 'title', 'description']);

const baseStyle = definePartsStyle((props: StyleFunctionProps) => {
  return {
    container: {
      borderColor: `${props.colorScheme}.400`,
      borderInlineEndRadius: 'md',
    },
    title: { fontWeight: 'bold' },
  };
});

const variants = {
  normal: definePartsStyle((props: StyleFunctionProps) => ({
    container: {
      color: `${props.colorScheme}.800`,
      bg: `${props.colorScheme}.50`,
      borderInlineStartWidth: '6px',
    },
  })),
  outline: definePartsStyle((props: StyleFunctionProps) => ({
    container: {
      color: `${props.colorScheme}.700`,
      borderWidth: '1px',
      borderInlineStartWidth: '6px',
    },
  })),
};

const sizes = {
  sm: definePartsStyle((props: StyleFunctionProps) => ({
    container: { py: 0.5, px: 2 },
    title: { fontSize: 'md' },
    description: { fontSize: 'sm' },
  })),
  md: definePartsStyle((props: StyleFunctionProps) => ({
    container: { py: 1.5, px: 2 },
    title: { fontSize: 'lg' },
    description: { fontSize: 'md' },
  })),
  lg: definePartsStyle((props: StyleFunctionProps) => ({
    container: { py: 3, px: 2 },
    title: { fontSize: '2xl' },
    description: { fontSize: 'xl' },
  })),
  xl: definePartsStyle((props: StyleFunctionProps) => ({
    container: { py: 3, px: 2 },
    title: { fontSize: '3xl' },
    description: { fontSize: '2xl' },
  })),
};

export const QuackAlertStyle = defineMultiStyleConfig({
  baseStyle,
  variants,
  sizes,
  defaultProps: {
    colorScheme: 'blue',
    size: 'md',
    variant: 'normal',
  },
});
