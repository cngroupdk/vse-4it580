import { extendTheme } from '@chakra-ui/react';

import { QuackAlertStyle } from './components/QuackAlertStyle';

export const theme = extendTheme({
  components: {
    QuackAlert: QuackAlertStyle,
  },
});
