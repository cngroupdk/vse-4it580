import { type ReactNode } from 'react';
import { Box, type ThemingProps, useMultiStyleConfig } from '@chakra-ui/react';

interface AlertOptions {
  title?: string;
  children?: ReactNode;
}

export interface QuackAlertProps
  extends AlertOptions,
    ThemingProps<'QuackAlert'> {}

export function QuackAlert(props: QuackAlertProps) {
  const styles = useMultiStyleConfig('QuackAlert', props);

  return (
    <Box __css={styles.container}>
      <Box __css={styles.title}>{props.title}</Box>
      <Box __css={styles.description}>{props.children}</Box>
    </Box>
  );
}
